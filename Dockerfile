FROM python:3.8.0-slim

WORKDIR /app
COPY . /app

CMD [ "python3" "app.py" ]